﻿using NetAPiRestful.Models.DataModels;


namespace NetAPiRestful.Services
{
    public interface IAdressesService
    {
        IEnumerable<Adress> GetAdressesByCountry(string country);
        IEnumerable<Adress> GetAdressesByCity(string city);
        IEnumerable<Adress> GetAdressesByCountryAndCity(string country, string city);
        IEnumerable<Adress> GetAdressesByPostalCode(string postalCode);
        IEnumerable<Adress> GetAdressByStreetNameAndStreetNumber(string streetName, int streetNumber);
        // TODO: Think some more
    }
}
