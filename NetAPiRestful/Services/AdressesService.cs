﻿using NetAPiRestful.Models.DataModels;

namespace NetAPiRestful.Services
{
    public class AdressesService : IAdressesService
    {

        public IEnumerable<Adress> GetAdressesByCity(string city)
        {
            throw new NotImplementedException();

        }

        public IEnumerable<Adress> GetAdressesByCountry(string country)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Adress> GetAdressesByCountryAndCity(string country, string city)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Adress> GetAdressesByPostalCode(string postalCode)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Adress> GetAdressByStreetNameAndStreetNumber(string streetName, int streetNumber)
        {
            throw new NotImplementedException();
        }
    }
}
