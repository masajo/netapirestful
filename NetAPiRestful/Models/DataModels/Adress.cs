﻿namespace NetAPiRestful.Models.DataModels
{
    public class Adress
    {
        public Guid Id { get; set; }
        public string? City { get; set; }
        public string? Country { get; set; } = "Spain";
        public string? PostalCode { get; set; }
        public string? StreetName { get; set; }
        public int StreetNumber { get; set; } = 1;
    }
}
