using NetAPiRestful.DataAccess;
using NetAPiRestful.Services;

var builder = WebApplication.CreateBuilder(args);

// Add Contexts to Services
builder.Services.AddDbContext<ContactsContext>();

// CORS Configuration
builder.Services.AddCors(options =>
    {
        options.AddPolicy(name: "CorsPolicy", builder =>
        {
            builder.AllowAnyOrigin();
            builder.AllowAnyMethod();
            builder.AllowAnyHeader();
        });
    }
);


// Add services to the container.

builder.Services.AddControllers();


// Add Services for Controllers
builder.Services.AddScoped<IAdressesService, AdressesService>();


// Add Swagger Generator Service
builder.Services.AddSwaggerGen();



// CONFIG APP
var app = builder.Build();


// Configure Swagger for Dev Env
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

// Say that our APP Will use CORS
app.UseCors("CorsPolicy");

app.Run();
