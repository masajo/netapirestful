﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetAPiRestful.Services;
using NetAPiRestful.Models.DataModels;

namespace NetAPiRestful.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdressesController : ControllerBase
    {

        private readonly IAdressesService _adressesService;

        public AdressesController(IAdressesService adressesService)
        {
            _adressesService = adressesService;
        }



        // CRUD





        // Extra from services
        [HttpGet]
        [Route("city/{city}")]
        [ProducesResponseType(200, Type=typeof(IEnumerable<Adress>))]
        [ProducesResponseType(404)]
        public IActionResult GetAdressesByCity(string city)
        {
            var adresses = _adressesService.GetAdressesByCity(city);
            if(adresses?.Any() == false)
            {
                return NotFound();
            }

            return Ok(adresses);
        }

    }
}
